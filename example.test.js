const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

test('Player should have 100 chips', () => {
    let hashname;
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return get('/chips', hashname);
    })
    .then(response => expect(response.chips).toEqual(100))
});

// Blad 1 - blad dla liczby = 11
for (let number of [2, 4, 6, 8, 10, 11]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); 
        })
        .then(response => post('/spin/' + number, hashname))
        .then(response => get('/chips', hashname)) 
        .then(response => expect(response.chips).toEqual(200))
    });
}

test.each([1,3,5,7])('Bet on odds should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/odd', hashname, { chips: 100 }); 
    })
    .then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(200))
});

test.each([1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34])('Zakład na 1 z trzech grup numerów złożonych z tuzina numerów, tuzin pierwszy, liczba = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname;
			return post('/bets/column/1', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(300))
});

test.each([2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35])('Zakład na 2 z trzech grup numerów złożonych z tuzina numerów, tuzin pierwszy, liczba = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname;
			return post('/bets/column/2', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(300))
});

test.each([3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36])('Zakład na 3 z trzech grup numerów złożonych z tuzina numerów, tuzin pierwszy, liczba = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname;
			return post('/bets/column/3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(300))
});

test.each([1, 2, 4, 5])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/1-2-4-5', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([2, 3, 5, 6])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/2-3-5-6', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(900))
});

test.each([4, 5, 7, 8])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/4-5-7-8', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([5, 6, 8, 9])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/5-6-8-9', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(900))
});

test.each([7, 8, 10, 11])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/7-8-10-11', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([8, 9, 11, 12])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/8-9-11-12', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([10, 11, 13, 14])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/10-11-13-14', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([11, 12, 14, 15])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/11-12-14-15', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(900))
});

test.each([13, 14, 16, 17])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/13-14-16-17', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([14, 15, 17, 18])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/14-15-17-18', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([16, 17, 19, 20])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/16-17-19-20', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([17, 18, 20, 21])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/17-18-20-21', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([19, 20, 22, 23])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/19-20-22-23', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([20, 21, 23, 24])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/20-21-23-24', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([22, 23, 25, 26])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/22-23-25-26', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([23, 24, 26, 27])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/23-24-26-27', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([25, 26, 28, 29])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/25-26-28-29', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([26, 27, 29, 30])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/26-27-29-30', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([28, 29, 31, 32])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/28-29-31-32', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([29, 30, 32, 33])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/29-30-32-33', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

// Blad 2 - brak testu
test.each([31, 32, 34, 35])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/31-32-34-35', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([32, 33, 35, 36])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/32-33-35-36', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([0, 1, 2, 3])('Zaklad na 4 sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/corner/0-1-2-3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(900))
});

test.each([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])('Zakład na jedną z trzech grup numerów złożonych z tuzina numerów. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/dozen/1', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(300))
});

// Blad 3 - wyrzuca blad dla liczby = 24
test.each([13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24])('Zakład na jedną z trzech grup numerów złożonych z tuzina numerów. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/dozen/2', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(300))
});

// Blad 4  - wyrzuca blad dla liczby = 36
test.each([25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])('Zakład na jedną z trzech grup numerów złożonych z tuzina numerów. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/dozen/3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(300))
});

// Blad 5 - liczba = 0 nie zalicza sie do liczb parzystych
test.each([0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36])('Zaklad na liczby parzyste. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/even', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(200))
});

// Blad 6 - liczba = 19 zalicza sie do liczb wysokich
test.each([19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])('Zakład na wysokie liczby. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/high', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(200))
});

// Blad 7 - brama testow = /bets/line
test.each([1, 2, 3, 4, 5, 6])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/1-2-3-4-5-6', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname))
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([4, 5, 6, 7, 8, 9])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/4-5-6-7-8-9', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(600))
});

test.each([7, 8, 9, 10, 11, 12])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/7-8-9-10-11-12', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([10, 11, 12, 13, 14, 15])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/10-11-12-13-14-15', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([16, 17, 18, 19, 20, 21])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/16-17-18-19-20-21', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(600))
});

test.each([19, 20, 21, 22, 23, 24])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/19-20-21-22-23-24-25', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([22, 23, 24, 25, 26, 27])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/22-23-24-25-26-27', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([25, 26, 27, 28, 29, 30])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/25-26-27-28-29-30', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([28, 29, 30, 31, 32, 33])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/28-29-30-31-32-33', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});
	
	test.each([31, 32, 33, 34, 35, 36])('Zaklad na 6 sasiadujacych ze soba numerow. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/line/31-32-33-34-35-36', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(600))
});

test.each([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18])('Zaklad na niskie numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/low', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(200))
});

test.each([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35])('Zaklad na numery nieparzyste. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/odd', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(200))
});

// Blad 8 - blad dla liczby = 12
test.each([1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36])('Zaklad na czerwone numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/red', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname))
    .then(response => expect(response.chips).toEqual(200))
});

// Blad 9 - zwraca inna wartosc wygranej (1:11), niz powinna byc wygrana (1:17) dla wszystkich testow /bets/split
test.each([0, 1])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/0-1', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([1, 2])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/1-2', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([1, 4])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/1-4', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([2, 5])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/2-5', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([3, 6])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/3-6', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([2, 3])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/2-3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([4, 7])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/4-7', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname))
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([5, 8])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/5-8', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([6, 9])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/6-9', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([3, 4])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/3-4', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([7, 10])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/7-10', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname))
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([8, 11])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/8-11', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([9, 12])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/9-12', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) 
    .then(response => get('/chips', hashname)) 
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([4, 5])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/4-5', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([10, 13])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/10-13', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([11, 14])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/11-14', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([12, 15])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/12-15', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([5, 6])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/5-6', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([13, 16])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/13-16', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([14, 17])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/14-17', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([15, 18])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/15-18', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([6,7])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/6-7', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([16, 19])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/16-19', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([17, 20])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/17-20', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([18, 21])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/18-21', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([7, 8])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/7-8', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([19, 22])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/19-22', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([20, 23])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/20-23', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([21, 24])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/21-24', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([8, 9])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/8-9', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([22, 25])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/22-25', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([23, 26])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/23-26', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([24, 27])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/24-27', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([9, 10])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/9-10', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([25, 28])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/25-28', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([26, 29])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/26-29', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([27, 30])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/27-30', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([10, 11])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/10-11', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([28, 31])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/28-31', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([29, 32])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/29-32', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([30, 33])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/30-33', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([11, 12])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/11-12', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([31, 34])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/31-34', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([32, 35])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/32-35', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([33, 36])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/33-36', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([12, 13])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/12-13', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([13, 14])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/13-14', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([14, 15])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/14-15', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([15, 16])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/15-16', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([16, 17])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/16-17', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([17, 18])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/17-18', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([18, 19])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/18-19', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([19, 20])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/19-20', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([20, 21])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/20-21', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([21, 22])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/21-22', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([22, 23])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/22-23', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([23, 24])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/23-24', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});


test.each([24, 25])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/24-25', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([25, 26])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/25-26', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([26, 27])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/26-27', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([27, 28])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/27-28', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([28, 29])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/28-29', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([29, 30])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/29-30', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([30, 31])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/30-31', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([31, 32])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/31-32', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([32, 33])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/32-33', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([33, 34])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/33-34', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([34, 35])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/34-35', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([35, 36])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/35-36', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([0, 2])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/0-2', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([0, 3])('Zaklad na dwa sasiadujace ze soba numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/split/0-3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([0])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/0', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([1])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/1', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([2])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/2', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([3])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});
*/
// Blad 10 - nie ma takiego testu
test.each([4])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/4', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([5])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/5', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([6])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/6', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([7])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/7', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([8])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/8', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([9])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/9', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([10])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/10', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([11])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/11', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([12])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/12', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([13])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/13', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([14])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/14', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([15])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/15', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([16])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/16', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([17])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/17', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([18])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/18', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([19])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/19', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([20])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/20', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([21])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/21', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([22])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/22', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([23])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/23', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([24])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/24', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([25])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/25', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([26])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/26', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([27])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/27', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([28])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/28', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([29])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/29', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([30])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/30', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([31])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/31', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([32])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/32', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([33])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/33', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([34])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/34', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([35])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/35', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
}); 

test.each([36])('Zaklad na pojedynczy numer. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/straight/36', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(3600))
});

test.each([1, 2, 3])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/1-2-3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([4, 5, 6])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/4-5-6', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});


test.each([7, 8, 9])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/7-8-9', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});


test.each([10, 11, 12])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/10-11-12', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});


test.each([13, 14, 15])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/13-14-15', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([16, 17, 18])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/16-17-18', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([19, 20, 21])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/19-20-21', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});


test.each([22, 23, 24])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/22-23-24', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([25, 26, 27])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/25-26-27', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([28, 29, 30])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/28-29-30', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([31, 32, 33])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/31-32-33', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([34, 35, 36])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/34-35-36', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});

test.each([0, 1, 2])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/0-1-2', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});


test.each([0, 2, 3])('Zakład na trzy sąsiadujące ze sobą kolejno w linii numery. Numer = %d', (number) => {
		return post('/players')
		.then(response => {
			hashname = response.hashname
			return post('/bets/street/0-2-3', hashname, { chips: 100 });
	})
	.then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(1200))
});